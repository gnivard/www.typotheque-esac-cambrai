var _BODY
var _BG
var _COLOR
var _ABOUT
var _TEXTS
var _SAMPLERS
var _CHARSET
var _BONBONS
var fontNames = []
var fonts
var _TITLE

var title = document.querySelector("nav h1");
var about = document.getElementById("about");
var closeAbout = about.querySelector('.close');
var closeFont = document.querySelector('.close-font');
var sizer = document.querySelector('.size');
var sampleHeaders

var isInViewport = function (elem) {
	var distance = elem.getBoundingClientRect();
	return (
		distance.top >= 0 &&
		distance.left >= 0 &&
		distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		distance.right <= (window.innerWidth || document.documentElement.clientWidth)
	);
}

function changeFontSize(demos){
  var fontSizeInput = document.getElementById("fontSize");
  fontSizeInput.addEventListener("input", function(){

    var newSize = this.value;
    demos.forEach(function(demo){
      demo.style.fontSize = newSize + "px";
    });
  });
}

function loadColor() {
  var cookies = document.cookie;
  cookies = cookies.split(";");
  if (cookies.length > 0) {
    cookies.forEach(function (cookie) {
      name = cookie.split("=")[0].replace(" ", "");
      value = cookie.split("=")[1].replace(" ", "");
      _BODY[0].style[name] = value;
      _ABOUT[0].style[name] = value;
      _CHARSET.forEach(set => {
        set.style[name] = value;
      })
      _TITLE.forEach(set => {
        set.style[name] = value;
      })

      if (name == "background") {
        _BG.value = value;
      }
      if (name == "color") {
        _COLOR.value = value;
      }
    });
  }
}

function changeColor(picker, elements, style_type) {
  picker.addEventListener("input", function () {
    elements.forEach(function (element) {
      element.style[style_type] = picker.value;
    });
    document.cookie = style_type + "=" + picker.value;
  });
}

function fillSamplers(newText) {
  var divide = newText.length / _SAMPLERS.length;
  _SAMPLERS.forEach(sampler => {
    sampler.innerHTML = '';
  })
  var i = 0;
  while(newText.length) {
    var range = newText.splice(0, divide.toFixed());
    range.forEach(word => {
      _SAMPLERS[i].innerHTML += word + ' ';
    })
    i++;
  }
}

function candy_explosion(){
	let i = Math.floor(Math.random() * _BONBONS.length);
	let bonbon = _BONBONS[i]
	if(bonbon.classList.contains( 'active' )){
		candy_explosion()
	}else {
		bonbon.classList.add('active')
	}
}

function candy_shop(){
	let windW = document.body.clientWidth
	let windH = document.body.offsetHeight

	_BONBONS.forEach(function(item, i){
			let bonbonLeft = Math.floor(Math.random() * windW);
			let bonbonTop = Math.floor(Math.random() * windH);
			item.style.left = bonbonLeft+'px'
			item.style.top = bonbonTop+'px'
	})
	setInterval(candy_explosion, 60000);

}


function showFonts(selected) {
  tags.forEach((tag) => {
    var tagValue = tag.getAttribute("data-value");
    if (tagValue == selected) {
      tag.closest(".font").classList.remove("hidden");
      tag.closest(".font").classList.add("show");
    }
  });
  fonts.forEach((font) => {
    if (!font.classList.contains("show")) {
      font.classList.remove("show");
      font.classList.add("hidden");
    }
  });
}

function hideFonts(selected) {
  var i = 0;
  filters.forEach((filter) => {
    i = filter.classList.contains("selected") ? i + 1 : i;
  });

  fonts.forEach((font) => {
    if (i == 0) {
      font.classList.remove("show");
      if (!font.classList.contains("show")) {
        font.classList.remove("hidden");
      }
    } else if (i > 0) {
      tags.forEach((tag) => {
        var tagValue = tag.getAttribute("data-value");
        if (tagValue == selected) {
          tag.closest(".font").classList.remove("show");
          tag.closest(".font").classList.add("hidden");
        }
      });
    }
  });
}


function generateText() {
  var allText = '';
  _TEXTS.forEach((text) => {
    var textcontent = text.innerHTML;
    allText += textcontent;
  })
  allWords = allText.split(' ');
  var newText = shuffleArray(allWords);
  return newText;
}

function shuffleArray(array) {
  let curId = array.length;
  let lastWord = '';
  if (document.body.classList.contains('mix-True')) {
    
    var articles = ['La', 'a', 'Le', 'des', 'Les', 'les', 'L\'', 'De', 'Du', 'D', 'de', 'du', 'le', 'la', 'l\'', 'une', 'un', 'à', 'se', 'ce', 'me'];
    // There remain elements to shuffle
    while (0 !== curId) {
      // Pick a remaining element
      let randId = Math.floor(Math.random() * curId);
      // Swap it with the current element.
      curId -= 1;
      let tmp = array[curId];
      if(!articles.includes(lastWord) && !articles.includes(tmp)) {
        array[curId] = array[randId];
        array[randId] = tmp;
        lastWord = tmp;
      } else {
        lastWord = tmp;
        // lastWord = lastWord
      }
    }
  }

  return array;
}


var lastWord;
function getWords(word) {
	var check = function(word){
		if (articles.includes(word) == true && articles.includes(lastword) == true ) {
			check(word)
		} else {
			lastWord = word
			return word
		}
	}
	var r0 = Math.floor(Math.random() * arr.length);
	var r1 = Math.floor(Math.random() * arr.length);
	var r2 = Math.floor(Math.random() * arr.length);
	var r3 = Math.floor(Math.random() * arr.length);
	return getWords(arr[0])+' '+getWords(arr[r1])+' '+getwords(arr[r2])+' '+getWords(arr[r3])
}

function randomTitle(){
	let titleSite = document.querySelector('#titleSite h1')
	let wordRdm = titleSite.getAttribute('data-random')
	var i = Math.floor(Math.random() * fontNames.length);
	let fontName = fontNames[i] 
	let SPAN = '<span id="martine" style="font-family: '+fontName+'">'+wordRdm+'</span>' 

	let text = titleSite.innerText.replace(wordRdm, SPAN)
	titleSite.innerHTML = text
	
}

function sortOut() {
	var fontsDict = document.getElementsByClassName('font') 
	var main = document.getElementsByClassName("main-fonts")[0]
	var divs = [];
	var l =  fontsDict.length
	for (var i = 0; i < l; ++i) {
		divs.push(fontsDict[i])
	}
	main.innerHTML = ''
	
	divs.sort(function(a, b) {
		return a.dataset.weight - b.dataset.weight
	});

	divs.forEach(function(el) {
		main.innerHTML += el.outerHTML
	})

	main.classList.add('visible')
}


function negativeFontOnHover() {
  sampleHeaders.forEach(header => {

    header.addEventListener('mouseenter', function () {
      var prevText = header.previousElementSibling.previousElementSibling;
      var fontColor = window.getComputedStyle(document.body).getPropertyValue("color");
      var bgColor = window.getComputedStyle(document.body).getPropertyValue("background-color");
			console.log(prevText)
      prevText.style.color = bgColor;
      prevText.style.backgroundColor = fontColor;
  })
  header.addEventListener('mouseleave', function () {
      var prevText = header.previousElementSibling.previousElementSibling;
      prevText.removeAttribute('style');
    })
  })
}

function focusOnFont(input) {
  closeFont.classList.add('visible');
  input.classList.remove('backward');
  input.classList.add('forward');
  _SAMPLERS.forEach(otherInput => {
    if (input != otherInput) {
      otherInput.classList.remove('forward');
      otherInput.classList.add('backward');
    }
  })
}

function lazyFont(fonts) {
  fonts.forEach(font => {
    if (isInViewport(font)) {
      var divFontFaceContent = font.querySelector('div.font-face').innerHTML;
      var styleFontFace = font.querySelector('style.font-face');
      styleFontFace.innerHTML = divFontFaceContent;
    }
  })
}

function setContentEditable(windowWidth, _SAMPLERS) {
  if (windowWidth < 600) {
    _SAMPLERS.forEach(sampler => {
      sampler.setAttribute('contenteditable', false);
    })
  } else if (windowWidth >= 600) {
    _SAMPLERS.forEach(sampler => {
      sampler.setAttribute('contenteditable', true);
    })
  }
}

document.addEventListener("DOMContentLoaded", function () {

  var windowWidth = window.innerWidth;

  _BODY = document.querySelectorAll("body");
  _ABOUT = document.querySelectorAll('#about');
  _BG = document.getElementById("backColor");
  _COLOR = document.getElementById("textColor");
	_TEXTS = document.querySelectorAll('#text-hidden li');
  _BONBONS = document.querySelectorAll('.bonbon');
	_TITLE = document.querySelectorAll('nav h1')
	
	sortOut()

	sampleHeaders = document.querySelectorAll('.sample-text-header');
	_SAMPLERS = document.querySelectorAll('.sample-text-edit');
  _CHARSET = document.querySelectorAll('.charset');


  title.addEventListener('click', function() {
    _ABOUT[0].classList.toggle('hidden');
  })

  closeAbout.addEventListener('click', function() {
    _ABOUT[0].classList.add('hidden');
  })

  var oldText
	var inputs = document.querySelectorAll('.sample-text');
  changeFontSize(inputs);

  _SAMPLERS.forEach(input => {
    input.addEventListener('focus', (e) =>{
      focusOnFont(input);
      oldText = input.textContent;
    })
  })

  closeFont.addEventListener('click', (e) => {
    closeFont.classList.remove('visible');
    _SAMPLERS.forEach(input => {
      if (input.classList.contains('forward')) {
        var newText = input.textContent;
         if (oldText != newText) {
          _SAMPLERS.forEach(input => {
            input.textContent = newText
          })
        }
        input.classList.remove('forward');
      }
      input.classList.remove('backward');
    })
  })

  var filters = document.querySelectorAll(".filter");
  var fonts = document.querySelectorAll(".font");

  filters.forEach((filter) => {
    filter.addEventListener("click", function () {
      var selected = filter.getAttribute("data-filter");
      if (filter.classList.contains("selected")) {
        filter.classList.remove("selected");
        hideFonts(selected);
      } else {
        filter.classList.add("selected");
        showFonts(selected);
      }
    });
  });

  lazyFont(fonts);

  var styles = document.querySelectorAll('.font-face')

  styles.forEach(function(item, i) {
    fontNames.push(item.getAttribute('data-font-family'))
  })

  var newText = generateText();
	fillSamplers(newText);



  changeColor(_BG, _BODY, "background");
  changeColor(_BG, _CHARSET, "background");
  changeColor(_BG, _ABOUT, "background");
  changeColor(_COLOR, _BODY, "color");
  changeColor(_COLOR, _ABOUT, "color");
  changeColor(_COLOR, _CHARSET, "color");
  changeColor(_BG, _TITLE, "background");
  changeColor(_COLOR, _TITLE, "color");
	
  loadColor();
	randomTitle()
	candy_shop()
  negativeFontOnHover();

  setContentEditable(windowWidth, _SAMPLERS);

})

window.addEventListener("resize", function () {
  var windowWidth = window.innerWidth;
	_SAMPLERS = document.querySelectorAll('.sample-text-edit');

  setContentEditable(windowWidth, _SAMPLERS);
})
