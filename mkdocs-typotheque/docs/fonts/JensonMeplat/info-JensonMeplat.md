---
title: Gabin Jenson Meplat
font_family: Gabin Jenson Meplat
designers:
 - [anonymous, ]
styles:
 - Italic
paths:
 - fonts/JensonMeplat/GabinJensonMeplat-Italic.otf
weight: 5
public: yes
---

 