---
title: ECART
font_family: ECART
project_url: 
license: 
license_url: 
designers:
 - [anonymous, ]
styles:
 - Bold
paths:
 - fonts/ECART/ECART-Bold.otf
tags:
ascender: 700
descender: -200
sample_text: "Récite moi l'alphabet."
weight: 5
public: no
---

 