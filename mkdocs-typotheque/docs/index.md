---
title: home 
---
Martine et les étudiant·e·s de l’École supérieure d’art et de communication de Cambrai créé·e·s des typographies.
Nos camarades dessinent des lettres, impriment des messages, examinent des caractères, jouent avec des balises et codent des mots.

Que de jolies choses se trouvent dans nos dossiers.

Entrez dans le bureau de Martine et découvrez de délicieuses typographies gratuites, originales et modifiables qui viendront nourrir vos créations.

* Licence: GNU GPl «explication nanana c’est libre»
* Contact: [mail@esac-cambrai.net](mail@esac-cambrai.net)
* D’autres Typothèque étudiantes: [ESA 75](url.url), [e162](urlt.urly), [la cambre](url.url).
* Voir le jeu de caractere
* Création: Luuse
* Membres typothèques: Gabin, Jerémy, Laureline, Ines, ect.
* Participants Workshop:   Nanana, Nanans, anana, anana
* Remerciments: Prof1 , Prof2 , Prof3
* Administration 1 et 2
* Avec le soutient du Crous Nord Pas-de-Calais et de l’ÉSAC
