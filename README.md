# Le bureau de Martine

## Alimentation de la typothèque

Rendez-vous sur la **web IDE** de Gitlab: 

![1](imgs_readme/1.png)

Cliquez sur **docs** puis sur les trois petits points à côté de **fonts** afin d'ouvrir la fenêtre d'édition. Cliquez ensuite sur **New directory**:

![2](imgs_readme/2.png) 

Entrez le nom de la typographie puis cliquez sur **Create directory**

Ensuite, cliquez sur le dossier que vous venez de créer, les trois petits points puis **Upload file**:

![3](imgs_readme/3.png)

Sélectionnez votre/vos fichier(s) ttf ou otf puis cliquez sur **ouvrir**.

Vous pouvez maintenant enregistrer vos modifications en cliquant sur **Commit**:

![4](imgs_readme/4.png)

**!!! ATTENTION !!!**, il faut sélectionner **Commit to main branch**! Vous pouvez ensuite cliquer sur Commit.

![5](imgs_readme/5.png)

Attendez quelques secondes que le programme génère les fichiers nécessaire. Un fois cela fait, rechargez la page et vous verrez apparaître un nouveau fichier dans le dossier de la fonte nommé **info-nomDeLaFonte.md**. Cliquez dessus afin de vérifier les informations et d'éventuellement l'éditer. Vous pouvez ajouter un·e designer et changer la valeur de sa graisse. Il est aussi possible de rendre la fonte publique ou non en changant la valeur de **public** par **yes** ou **no**.

![6](imgs_readme/6.png)

**Attention**, d'une manière générale, veuillez à respecter la syntaxe existante. 

cliquez de nouveau sur **Commit**. N'oubliez pas encore une fois de sélectionner **Commit to main branch**!

![7](imgs_readme/7.png)

Vous pouvez ensuite vous rendre sur le site et vous verrez apparaître votre fonte!

![8](imgs_readme/8.png)

## Modifier le contenu

Pour modifier le contenu du texte de démonstration ou le titre du site,, rendez vous de nouveau sur la web IDE de Gitlab.

![9](imgs_readme/1.png)

Ouvrez le fichier **mkdocs.yml** situé dans le dossier **mkdocs-typotheque**.

![10](imgs_readme/9.png)

Vous pouvez ici changer le nom du site grâce à *site_name*, le sous-titre avec *site_subtitle* ainsi que le ou les texte(s) de démonstration avec *texts*. Il est important de respecter la syntaxe, à savoir, placer un tirer avant le texte et mettre celui-ci entre guillements.

La dernière valeur *mix* permet de choisir s'il l'on souhaite que les textes soient mélangés, écriez *true* pour mélanger, *false* pour les mettre à la suite.

![11](imgs_readme/10.png)

 Commitez ensuite vos modification en pensant bien à sélectionner **Commit to main branch**.

Pour modifier le à propos, ouvrez le fichier *index.md* situé dans le dossier *mkdocs-typotheque > docs*. Celui-ci est formaté en [markdown](https://assemble.io/docs/Cheatsheet-Markdown.html). Pour le colophon, utilisez des listes.

![12](imgs_readme/11.png)

 Commitez ensuite vos modification en pensant bien à sélectionner **Commit to main branch**.

Voilà.